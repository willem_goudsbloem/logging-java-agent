package com.databled.agent.properties;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.databled.agent.properties.DaProperties;
import com.databled.agent.properties.DaPropertiesFromFile;



public class TestDaPropertiesFromFile {

	DaProperties daProperties;
	
	@Before
	public void setUp() throws Exception {
		daProperties = new DaPropertiesFromFile();
	}

	@Test
	public void testDaPropertiesFromFile() {
		assertNotNull(daProperties);
	}

	@Test
	public void testGetProp(){
		@SuppressWarnings("unchecked")
		List<String> list = daProperties.getPropS("excludes", List.class);
		assertNotNull(list);
		assertTrue(list.contains("java/lang"));
		assertTrue(list.contains("sun"));
		
	}
	
	public void testGetPropS() {
		fail("Not yet implemented");
	}

}
