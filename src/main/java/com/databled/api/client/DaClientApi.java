package com.databled.api.client;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DaClientApi {
	
	static Logger log = LoggerFactory.getLogger(DaClientApi.class);
	
	static String FORMAT_STRING = "class:{}|method:{}|value:{}|type:{}";
	
	
	// int = I, long = J, boolean = Z, float = F, double = D, char = C
	public static void submit(String className, String methodName, int value){
		log.debug(FORMAT_STRING,className, methodName, value);
	}
	
	public static void submit(String className, String methodName, long value){
		log.debug(FORMAT_STRING,className, methodName, value);
	}

	public static void submit(String className, String methodName, boolean value){
		log.debug(FORMAT_STRING,className, methodName, value);
	}
	
	public static void submit(String className, String methodName, float value){
		log.debug(FORMAT_STRING,className, methodName, value);
	}
	
	public static void submit(String className, String methodName, double value){
		log.debug(FORMAT_STRING,className, methodName, value);
	}
	
	public static void submit(String className, String methodName, char value){
		log.debug(FORMAT_STRING,className, methodName, value);
	}
	
	public static void submit(String className, String methodName, Object value){
		String type = null;
		if(value instanceof java.util.Date) {
			type="long (java.util.Date.getTime())";
			value = ((java.util.Date) value).getTime();
		}

		log.debug(FORMAT_STRING,className, methodName, value, type);
	}
	
	public static void performance(String className, String methodName, long time){
		log.debug(FORMAT_STRING,className, methodName, time, "performance");
	}

}
