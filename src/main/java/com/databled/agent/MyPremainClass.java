package com.databled.agent;

import java.lang.instrument.Instrumentation;

import com.databled.agent.selection.ClassListGenerator;

public class MyPremainClass { 

	public static void premain(String agentArgs, Instrumentation inst){		
		inst.addTransformer(new ClassListGenerator());
	}
	
	
}
