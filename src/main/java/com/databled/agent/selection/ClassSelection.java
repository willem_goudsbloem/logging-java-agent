/**
 * 
 */
package com.databled.agent.selection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author willem.goudsbloem@gmail.com
 *
 */
public class ClassSelection {
	
	public final static List<ClassSelection> list = new ArrayList<ClassSelection>();
	
	public ClassSelection(String className, boolean selected) {
		super();
		this.className = className;
		this.selected = selected;
	}
	
	private String className;
	private boolean selected = false;
	
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * @return is this class selection selected
	 */
	public boolean isSelected() {
		return selected;
	}
	/**
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
