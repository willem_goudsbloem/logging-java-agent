package com.databled.agent.selection;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.List;

import org.objectweb.asm.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.databled.agent.properties.DaProperties;

public class ClassListGenerator implements ClassFileTransformer {

	Logger log = LoggerFactory.getLogger(ClassListGenerator.class);

	public byte[] transform(ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {

		if (classNameIncluding(className)) {
			ClassReader cr = new ClassReader(classfileBuffer);
			ClassWriter cw = new ClassWriter(cr, ClassWriter.COMPUTE_MAXS);
			ClassVisitor cp = new ClassVisitor(Opcodes.ASM5, cw) {

				public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
					//log.debug(name + " extends " + superName);
					super.visit(version, access, name, signature, superName, interfaces);
				}

				@Override
				public MethodVisitor visitMethod(int access, String name, final String desc, String signature, String[] exceptions) {
					MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
					if (name.matches(DaProperties.props.getPropS("methodRegex", String.class))) {
						//log.debug(access + ":" + name + ":" + desc + ":" + signature);
						return new MethodVisitor(Opcodes.ASM5, mv) {

							@Override
							public void visitCode() {
								mv.visitCode();
								mv.visitVarInsn(Opcodes.ALOAD, 0);
								mv.visitVarInsn(Opcodes.ALOAD, 0);
								mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
								mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);
								mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
								mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Thread", "getStackTrace", "()[Ljava/lang/StackTraceElement;", false);
								mv.visitInsn(Opcodes.ICONST_1);
								mv.visitInsn(Opcodes.AALOAD);
								mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/StackTraceElement", "getMethodName", "()Ljava/lang/String;", false);
								Type type = Type.getArgumentTypes(desc)[0];
								mv.visitVarInsn(type.getOpcode(Opcodes.ILOAD), 1);
								mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/databled/api/client/DaClientApi", "submit",
										"(Ljava/lang/String;Ljava/lang/String;"+ checkType(type.getDescriptor()) +")V", false);
								mv.visitEnd();
							}
							
							/**
							 * checks if the description is a primitive data type,
							 * if not the type is an Object (including String)
							 * 
							 * @param desc
							 * @return
							 */
							String checkType(String desc){
								// int = I, long = J, boolean = Z, float = F, double = D, char = C
								if (desc.matches("(I|J|Z|F|D|C)")) return desc;
								return "Ljava/lang/Object;";
							}

						};
					}
					return mv;
				}

			};
			cr.accept(cp, 0);
			return cw.toByteArray();
		}
		return classfileBuffer;
	}

	boolean classNameExcluding(String className) {
		@SuppressWarnings("unchecked")
		List<String> excludes = DaProperties.props.getPropS("excludes", List.class);
		for (String exclude : excludes) {
			if (className.contains(exclude)) {
				return false;
			}
		}
		return true;
	}

	boolean classNameIncluding(String className) {
		@SuppressWarnings("unchecked")
		List<String> includes = DaProperties.props.getPropS("includes", List.class);
		for (String include : includes) {
			if (className.contains(include)) {
				return true;
			}
		}
		return false;
	}

}
