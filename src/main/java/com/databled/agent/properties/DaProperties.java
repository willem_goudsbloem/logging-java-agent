/**
 * 
 */
package com.databled.agent.properties;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author willem.goudsbloem@gmail.com
 * @created May 15, 2015 
 *
 * trait to retrieve program properties
 *
 */
public abstract class DaProperties {
	
	
	protected Map<String, Object> map = new HashMap<String, Object>();


	/**
	 * get Propery from passed key
	 * 
	 * @param key
	 * @return Object
	 */
	public Map<String, Object> getProp(String key) {
		// TODO Auto-generated method stub
		return map;
	}


	/**
	 * convenience method
	 * get property in passed type
	 * @param <T>
	 * 
	 * @param key
	 * @return
	 */

	public <T> T getPropS(String key, Class<T> type) throws ClassCastException {
		return type.cast(map.get(key));
	}
	
	/**
	 * @todo remove hardcode, but it is fine for now.
	 */
	public static DaProperties props = new DaPropertiesFromFile();
	
	
}
