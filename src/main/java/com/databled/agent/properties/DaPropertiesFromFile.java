/**
 * databled.com java-agent
 */
package com.databled.agent.properties;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author willem.goudsbloem@gmail.com
 * @created May 15, 2015 
 *
 */
public class DaPropertiesFromFile extends DaProperties {
	
	final static Logger log = LoggerFactory.getLogger(DaPropertiesFromFile.class);
	
	static final String PROPERTIES_FILE = "da-properties.json";
		
	public DaPropertiesFromFile(){
		Reader reader = getReaderFromPropertiesFile(PROPERTIES_FILE);
		parseFile(reader);
	}
	
	/**
	 * Returns a reader from a file name in the class path
	 * 
	 * @param fileName of the properties file
	 * @return character stream reader
	 */
	Reader getReaderFromPropertiesFile(String fileName) {
		Reader reader = null;
		InputStream in = getClass().getResourceAsStream("/"+fileName);
		reader = new BufferedReader(new InputStreamReader(in));
		return reader;
	}
	
	/**
	 * { properties:
	 *  { includes : [value1, value2] }
	 * }
	 * 
	 * @param reader
	 */
	@SuppressWarnings("unchecked")
	void parseFile(Reader reader){
		JSONObject root = (JSONObject) JSONValue.parse(reader);
		JSONObject properties = (JSONObject) root.get("properties");
		map.put("excludes", (List<String>) properties.get("excludes"));
		map.put("includes",  (List<String>) properties.get("includes"));
		map.put("methodRegex",  (String) properties.get("methodRegex"));
	}
	


}
